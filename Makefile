UNAME := $(shell uname)


LIB_PATH=/usr/local/lib
INC_PATH=/usr/local/include
BIN_PATH=/usr/local/bin

LUA_LIB= -L$(LIB_PATH) -llua
LUA_INC= -I$(INC_PATH)
#TODO - now its working, but need proper ones
MYSQL_INC= -I/usr/local/include/mysql
MYSQL_LIB= -L/usr/local/lib -lmysqlclient -lz
EXTRACFLAGS= -std=c99  -undefined dynamic_lookup -fPIC

INC= $(LUA_INC) $(MYSQL_INC)
LIB= $(LUA_LIB) $(MYSQL_LIB)
WARN= -Wall
CFLAGS= -O2 $(WARN) $(INC) $(LIB) $(EXTRACFLAGS)



MYNAME= mysql
MYLIB= $(MYNAME)
T= $(MYLIB).so
OBJS= lua_$(MYLIB).o

all: so

o:	$(MYLIB).o

so:	$T

$T:	$(OBJS)
	$(CC) $(CFLAGS) -o $@ -shared $(OBJS)

clean:
	rm -f $T $(OBJS)

