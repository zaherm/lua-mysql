#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "mysql.h"
#include "lua.h"
#include "lauxlib.h"

#define LUAMYSQL_VERSION "lua-mysql 0.0.1"
#define LUAMYSQL_COPYRIGHT "Copyright (C) 2013, Zaher Marzuq"
#define LUAMYSQL_DESCRIPTION "MySQL binding for Lua"

#include "lua_mysql.h"
#include "src/lm_misc.c"
#include "src/lm_options.c"
#include "src/lm_connection.c"
#include "src/lm_result.c"
#include "src/lm_statement.c"

/*
** Create metatables for each class of object.
*/
static void lm_create_metatables(lua_State *L) {
  lm_createmeta(L, LUAMYSQL_CONNECTION, lm_connection_methods);
  lm_createmeta(L, LUAMYSQL_RESULT, lm_result_methods);
  lm_createmeta(L, LUAMYSQL_STATEMENT, lm_statement_methods);
  lua_pop(L, 2);
}


static const struct luaL_reg thislib[] = {
  {"connection", lm_connection_init},
  {"result", lm_result_init},
  {"statement", lm_statement_init},
  {"hexstring", lm_hex_string},
  {"get_client_info", lm_get_client_info},
  {"get_client_version", lm_get_client_version},
  {NULL, NULL}
};

LUALIB_API int luaopen_mysql(lua_State *L) {
  lm_create_metatables(L);
  lua_newtable(L);
  luaL_setfuncs(L, thislib, 0);

  lm_options_table(L);

  lua_pushliteral(L, LUAMYSQL_VERSION);
  lua_setfield(L, -2, "_VERSION");
  lua_pushliteral(L, LUAMYSQL_COPYRIGHT);
  lua_setfield(L, -2, "_COPYRIGHT");
  lua_pushliteral(L, LUAMYSQL_DESCRIPTION);
  lua_setfield(L, -2, "_DESCRIPTION");

  return 1;
}

