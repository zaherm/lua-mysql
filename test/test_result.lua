local mysql = require("mysql")

math.randomseed(os.time())
local queries = {
  drop = [[
    DROP TABLE IF EXISTS luamysql_result
    ]],
  create = [[
    CREATE TABLE luamysql_result (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        r_text TEXT,
        r_bigint BIGINT,
        r_float FLOAT,
        r_tinyint TINYINT,
        r_timedate DATETIME
        )
  ]],
}

function before()
  local c = mysql.connection()
  assert(c)
  assert(c:connect("localhost", "root", "1", "luamysql"))
  assert(c:query(queries.drop))
  assert(c:error() == "")
  assert(c:query(queries.create))
  assert(c:error() == "")
  return c
end

function after(c)
  assert(c:close())
  return c
end

local tests = {}
tests.fetch_field = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["select"] = assert(c:query("SELECT id,r_float FROM luamysql_result WHERE id = 1"))
  local res = mysql.result(c)
  results["create_result"] = assert(res)
  assert(type(res) == "userdata")
  local field
  for _, f in ipairs({"id", "r_float"}) do
    field = res:fetch_field()
    results["fetch_field_"..f.."_name"] = assert(field.name == f)
    results["fetch_field_"..f.."_table"] = assert(field.table == "luamysql_result")
    results["fetch_field_"..f.."_db"] = assert(field.db == "luamysql")
    end
  assert(type(res) == "userdata")
  return results
end

tests.fetch_field_direct = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["select"] = assert(c:query("SELECT * FROM luamysql_result WHERE id = 1"))
  local res = mysql.result(c)
  results["create_result"] = assert(res)
  assert(type(res) == "userdata")
  local field = res:fetch_field_direct(1) --keeping with lua index at 1
  results["fetch_field_name"] = assert(field.name == "id")
  results["fetch_field_table"] = assert(field.table == "luamysql_result")
  results["fetch_field_db"] = assert(field.db == "luamysql")
  assert(type(res) == "userdata")
  return results
end

tests.fetch_fields = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["select"] = assert(c:query("SELECT r_text,r_bigint FROM luamysql_result WHERE id = 1"))
  local res = mysql.result(c)
  results["create_result"] = assert(res)
  assert(type(res) == "userdata")
  local fields = res:fetch_fields()
  for i,f in ipairs({"r_text", "r_bigint"}) do
    results["fetch_fields_"..f.."_name"] = assert(fields[i].name == f)
    results["fetch_fields_"..f.."_table"] = assert(fields[i].table == "luamysql_result")
    results["fetch_fields_"..f.."_db"] = assert(fields[i].db == "luamysql")

  end
  return results
end

tests.fetch_lengths = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["select"] = assert(c:query("SELECT r_text,r_bigint FROM luamysql_result WHERE id = 1"))
  local res = mysql.result(c)
  results["create_result"] = assert(res)
  assert(type(res) == "userdata")
  local row = res:fetch_row({}, "a")
  local lengths = res:fetch_lengths()
  local map = {
    { name = "r_text", length = 10 },
    { name = "r_bigint", length = 13 }
  }
  for i,f in ipairs(map) do
    results["fetch_lengths_"..(f.name).."_name"] = assert(lengths[i] == f.length )
  end
  return results
end


tests.insert_and_select = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["insert_id"] = assert(c:insert_id() == 1)
  results["select"] = assert(c:query("SELECT * FROM luamysql_result WHERE id = 1"))
  local result = mysql.result(c)
  local row = result:fetch_row({}, "a")
  results["fetch_row_type"] = assert(type(row) == "table")
  results["row_content"] = assert(row.r_text == "dummy text")
  return results
end

tests.num_rows = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["insert_id"] = assert(c:insert_id() == 1)
  results["select"] = assert(c:query("SELECT * FROM luamysql_result WHERE id = 1"))
  local result = mysql.result(c)
  results["num_rows"] = assert(result:num_rows() == 1)
  return results
end

tests.num_fields = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["insert_id"] = assert(c:insert_id() == 1)
  results["select"] = assert(c:query("SELECT id, r_text FROM luamysql_result WHERE id = 1"))
  local result = mysql.result(c)
  results["num_fields"] = assert(result:num_fields() == 2)
  return results
end

tests.connection_result = function(c)
  local results = {}
  local insert = [[
    INSERT INTO luamysql_result(r_text, r_bigint, r_float, r_tinyint, r_timedate)
    VALUES('dummy text', 1237214913123, 12111.1, 1, '2013-06-01 01:01:01')
  ]]
  results["insert"] = assert(c:query(insert))
  results["insert_id"] = assert(c:insert_id() == 1)
  results["select"] = assert(c:query("SELECT id, r_text FROM luamysql_result WHERE id = 1"))
  local result = c:result()
  results["num_fields"] = assert(result:num_fields() == 2)
  return results
end



for test, func in pairs(tests) do
  print(string.rep("-", 50))
  print("Running Test["..test.."]")
  local c = before()
  local results = func(c)
  for k,v in pairs(results) do
    print("  "..k..": "..(k and "pass" or "fail"))
  end
  after(c)
end

