local mysql = require("mysql")

math.randomseed(os.time())

function before()
  local c = mysql.connection()
  assert(c)
  assert(c:connect("localhost", "root", "1", "luamysql"))
  assert(c:error() == "")
  assert(c:ping() == 0)
  return c
end

function after(c)
  assert(c:close())
  return c
end

local tests = {}

tests.connection = function(c)
  local results = {}
  results["verify_created"] = assert(c)
  return results
end

tests.escape = function(c)
  local results = {}
  local out = c:escape("a'b")
  results["escape_string"] = assert(out == "a\\'b")
  return results
end


tests.character_set_name = function(c)
  local results = {}
  results["verify_utf8"] = assert(c:character_set_name() == "utf8")
  return results
end

tests.errno = function(c)
  local results = {}
  results["verify_0"] = assert(c:errno() == 0)
  return results
end

tests.autocommit = function(c)
  local results = {}
  results["mode_1"] = assert(c:autocommit(true) == 0)
  results["mode_0"] = assert(c:autocommit(false) == 0)
  return results
end

tests.select_db = function(c)
  local results = {}
  results["select_current"] = assert(c:select_db("luamysql") == 0)
  results["select_dummy"] = assert(c:select_db("dummy_db") ~= 0)
  return results
end

tests.stat = function(c)
  local results = {}
  results["result_type_string"] = assert(type(c:stat()) == "string")
  return results
end

tests.get_character_set_info = function(c)
  local results = {}
  results["set_utf8"] = assert(c:character_set_name() == "utf8")
  local info = c:get_character_set_info()
  results["verify_return_value"] = assert(type(info) == "table")
  results["verify_csname_utf8"] = assert(info.csname == "utf8")
  return results
end

tests.get_host_info = function(c)
  local results = {}
  results["verify_localhost"] = assert(string.find(c:get_host_info(), "Localhost"))
  return results
end

tests.get_proto_info = function(c)
  local results = {}
  results["verify_return_number"] = assert(type(c:get_proto_info()) == "number")
  return results
end

tests.get_server_info = function(c)
  local results = {}
  results["verify_return_string"] = assert(type(c:get_server_info()) == "string")
  return results
end

tests.get_server_version = function(c)
  local results = {}
  results["verify_return_number"] = assert(type(c:get_server_version()) == "number")
  return results
end

tests.get_ssl_cipher = function(c)
  local results = {}
  results["verify_no_cipher"] = assert(c:get_ssl_cipher() == nil)
  return results
end

tests.info = function(c)
  local results = {}
  results["verify_return_nil"] = assert(c:info() == nil)
  return results
end

tests.field_count = function(c)
  local results = {}
  results["run_show_tables"] = assert(c:query("SHOW TABLES"))
  results["verify_field_count_1"] = assert(c:field_count() == 1)
  return results
end

for test, func in pairs(tests) do
  print(string.rep("-", 50))
  print("Running Test["..test.."]")
  local c = before()
  local results = func(c)
  for k,v in pairs(results) do
    print("  "..k..": "..(k and "pass" or "fail"))
  end
  after(c)
end
