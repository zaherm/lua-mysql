local mysql = require("mysql")

math.randomseed(os.time())

local types = {
  FLOAT = function()
    return math.random()*1000;
  end,
  INT = function()
  return math.ceil(math.random()*1000)
  end,
  BIGINT = function()
    return math.floor(math.random()*100000000000000)
  end,
  DOUBLE = function()
    return math.random()*10000000
  end,
  TINYINT = function()
    return (math.random()*10 > 5) and 1 or 0
  end,
  TEXT = function()
    local len =  10 --000 --math.random()*999--999
    return string.rep("a", len)
  end,
  LONG = function()
    return math.floor(math.random()*100000000)
  end
}

local queries = {
  drop = [[
    DROP TABLE IF EXISTS luamysql_%s
    ]],
  create = [[
    CREATE TABLE luamysql_%s (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        data %s
        )
  ]],
  insert = [[
    INSERT INTO luamysql_%s(data) VALUES(?)
    ]],
  select = [[
    SELECT * FROM luamysql_%s WHERE data = ?
    ]]
}

local special_queries = {
  FLOAT ={
    select =  [[
      SELECT * FROM luamysql_%s WHERE CAST(data AS DECIMAL) = CAST(? AS DECIMAL)
      ]]
  },
  DOUBLE = {
    select =  [[
      SELECT * FROM luamysql_%s WHERE CAST(data AS DECIMAL) = CAST(? AS DECIMAL)
      ]]
  }
}


function test_prepared_statements()
  local c = mysql.connection()
  assert(c)
  c:connect("localhost", "root", "1", "luamysql")
  assert(c:error() == "")
  assert(c:ping() == 0)
  local statement = mysql.statement(c)
  print(c._TYPE);
  assert(c:ping())
  local v, r, ok
  for t, g in pairs(types) do
    v = g()
    print(string.rep("-", 40))
    print("test["..t.."]: start")
    ok = assert(c:query(string.format(queries.drop, t)))
    print("test["..t.."]: drop table", ok and "OK")
    ok = assert(c:query(string.format(queries.create, t, t)))
    print("test["..t.."]: create table", ok and "OK")
    ok = assert(c:ping())
    print("test["..t.."]: ping", ok and "OK")
    ok = assert(statement:prepare(string.format(queries.insert, t)))
    print("test["..t.."]: prepare insert", ok and "OK")
    ok = assert(statement:bind(v))
    print("test["..t.."]: bind insert", ok and "OK")
    ok = assert(statement:execute())
    print("test["..t.."]: execute", ok and "OK")
    ok = assert(statement:error() == "")
    print("test["..t.."]: no error", ok and "OK")


    ok = assert(statement:reset()) --other wise commands will be out of sync
    print("test["..t.."]: reset", ok and "OK")
    if special_queries[t] then
      ok = assert(statement:prepare(string.format(special_queries[t].select, t)))
    else
      ok = assert(statement:prepare(string.format(queries.select, t)))
    end
    print("test["..t.."]: preapre select", ok and "OK")
    ok = assert(statement:bind(v))
    print("test["..t.."]: bind select", ok and "OK")

    ok = assert(statement:execute())
    print("test["..t.."]: execute select", ok and "OK")

    r = statement:fetch({}, "a")
    print("test["..t.."]: fetch", ok and "OK")
    ok = assert(type(r) == "table")

    print("test["..t.."]: fetch result", ok and "OK")
    if special_queries[t] and r.data ~= v then
      ok = false ---assert(r.data == v)
    end
    print("test["..t.."]: fetch compare", ok and "OK")

    ok = assert(statement:reset()) --other wise commands will be out of sync
    print("test["..t.."]: reset", ok and "OK")
  end
  assert(c:close())
end





test_prepared_statements()


  --force gar
for i = 1, 10000 do
  if i%10 == 0 then
    collectgarbage"collect"
  end
--  string.rep("x", i)
end

os.execute('sleep 3')
