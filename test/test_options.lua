local mysql = require("mysql")

function get_pid(c)
  assert(c:query("SHOW PROCESSLIST"))
  local result = mysql.result(c)
  local r = result:fetch_row({}, "a")
  local pid
  while r do
    if r.db == "luamysql" then
      pid = r.Id
      break
    end
    r = result:fetch_row({}, "a")
  end
  return pid
end

function test_reconnect(reconnect)
  local pid1, pid
  local c = mysql.connection({
    reconnect = reconnect,
  });
  assert(c:connect("localhost", "root", "1", "luamysql"));
 -- assert(c:ping())
  pid1 = get_pid(c)
--  assert(pid1 ~= nil)
  assert(c:query("KILL "..tostring(pid1)))
  if c:ping() == 0 then
    pid2 = get_pid(c)
  end
--  assert(c:close())
  return pid1, pid2
end

local pid1, pid2 = nil, nil

pid1, pid2 = test_reconnect(false)
assert(pid1 ~= nil)
assert(type(pid2) == "nil")
pid1, pid2 = test_reconnect(true)
assert(pid1 ~= nil)
assert(pid2 ~= nil)
assert(pid1 ~= pid2)

local base_dir = "/usr/local/etc/mysql/"
local c = mysql.connection();
c:ssl_set(base_dir.."client-key.pem", base_dir.."client-cert.pem")
pcall(function()
  c:connect("localhost", "root", "1", "luamysql")
end)
assert(c:ping())

print(c:error())

