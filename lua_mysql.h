#define LUAMYSQL_CONNECTION "luamysql.connection"
#define LUAMYSQL_RESULT "luamysql.result"
#define LUAMYSQL_STATEMENT "luamysql.statement"
#define LUAMYSQL_OPTIONS "luamysql.options"

/* connection struct */
typedef struct {
  MYSQL *mysql;
  int closed;
} lm_connection;

/* result struct */
typedef struct {
  MYSQL_RES *res;
} lm_result;

/* statement struct */
typedef struct {
  MYSQL_STMT *stmt;
  MYSQL_RES *res;
  /* array of metadata per bind param */
  struct {
    int n;
    unsigned long *length;
    MYSQL_BIND *bind;
    MYSQL_TIME *buffer;
  } param;
  struct {
    int n;
    MYSQL_BIND *bind;
    my_bool *is_null;
    unsigned long *length;
  } result;
  MYSQL_FIELD *fields;
} lm_statement;

/* options */
enum lm_option {
  LUAMYSQL_OPTION_STRING,
  LUAMYSQL_OPTION_NUMBER,
  LUAMYSQL_OPTION_BOOLEAN,
  LUAMYSQL_OPTION_NULL
};

static struct {
  const char* name;
  enum mysql_option mysql_opt;
  enum lm_option lm_opt;
} lm_options_map[] = {
  {"connect_timeout", MYSQL_OPT_CONNECT_TIMEOUT, LUAMYSQL_OPTION_NUMBER},
  {"compress", MYSQL_OPT_COMPRESS, LUAMYSQL_OPTION_NULL},
  {"named_pipe", MYSQL_OPT_NAMED_PIPE, LUAMYSQL_OPTION_STRING},
  {"init_command", MYSQL_INIT_COMMAND, LUAMYSQL_OPTION_STRING},
  {"read_default_file", MYSQL_READ_DEFAULT_FILE, LUAMYSQL_OPTION_STRING},
  {"read_default_group", MYSQL_READ_DEFAULT_GROUP, LUAMYSQL_OPTION_STRING},
  {"set_charset_dir", MYSQL_SET_CHARSET_DIR, LUAMYSQL_OPTION_STRING},
  {"set_charset_name", MYSQL_SET_CHARSET_NAME, LUAMYSQL_OPTION_STRING},
  {"local_infile", MYSQL_OPT_LOCAL_INFILE, LUAMYSQL_OPTION_STRING},
  {"protocol", MYSQL_OPT_PROTOCOL, LUAMYSQL_OPTION_STRING},
  {"shared_memory_base_name", MYSQL_SHARED_MEMORY_BASE_NAME, LUAMYSQL_OPTION_STRING},
  {"read_timeout", MYSQL_OPT_READ_TIMEOUT, LUAMYSQL_OPTION_NUMBER},
  {"write_timeout", MYSQL_OPT_WRITE_TIMEOUT, LUAMYSQL_OPTION_NUMBER},
  {"use_result", MYSQL_OPT_USE_RESULT, LUAMYSQL_OPTION_STRING},
  {"use_remote_connection", MYSQL_OPT_USE_REMOTE_CONNECTION, LUAMYSQL_OPTION_NULL},
  {"use_embedded_connection", MYSQL_OPT_USE_EMBEDDED_CONNECTION, LUAMYSQL_OPTION_NULL},
  {"guess_connection", MYSQL_OPT_GUESS_CONNECTION, LUAMYSQL_OPTION_NULL},
  {"set_client_ip", MYSQL_SET_CLIENT_IP, LUAMYSQL_OPTION_STRING},
  {"secure_auth", MYSQL_SECURE_AUTH, LUAMYSQL_OPTION_BOOLEAN},
  {"report_data_truncation", MYSQL_REPORT_DATA_TRUNCATION, LUAMYSQL_OPTION_STRING},
  {"reconnect", MYSQL_OPT_RECONNECT, LUAMYSQL_OPTION_BOOLEAN},
  {"ssl_verify_server_cert", MYSQL_OPT_SSL_VERIFY_SERVER_CERT, LUAMYSQL_OPTION_BOOLEAN},
  {"plugin_dir", MYSQL_PLUGIN_DIR, LUAMYSQL_OPTION_STRING},
  {"default_auth", MYSQL_DEFAULT_AUTH, LUAMYSQL_OPTION_STRING},
  {NULL, MYSQL_OPT_COMPRESS, LUAMYSQL_OPTION_NUMBER}
};

/* connection functions */
static int lm_connection_init(lua_State *L);
static int lm_connection_connect(lua_State *L);
static int lm_connection_close(lua_State *L);
static int lm_connection_commit(lua_State *L);
static int lm_connection_error(lua_State *L);
static int lm_connection_errno(lua_State *L);
static int lm_connection_query(lua_State *L);
static int lm_connection_ping(lua_State *L);
static int lm_connection_escape(lua_State *L);
static int lm_connection_insert_id(lua_State *L);
static int lm_connection_affected_rows(lua_State *L);
static int lm_connection_autocommit(lua_State *L);
static int lm_connection_rollback(lua_State *L);
static int lm_connection_character_set_name(lua_State *L);
static int lm_connection_ssl_set(lua_State *L);
static int lm_connection_select_db(lua_State *L);
static int lm_connection_stat(lua_State *L);
static int lm_connection_sqlstate(lua_State *L);
static int lm_connection_field_count(lua_State *L);
static int lm_connection_get_host_info(lua_State *L);
static int lm_connection_get_proto_info(lua_State *L);
static int lm_connection_get_server_info(lua_State *L);
static int lm_connection_get_server_version(lua_State *L);

static int lm_connection_gc(lua_State *L);

/* misc; don't need db connection */
static int lm_hex_string(lua_State *L);
static int lm_options_table(lua_State *L);

/* result functions */
static int lm_result_init(lua_State *L);
static int lm_result_fetch_row(lua_State *L);
static int lm_result_free(lua_State *L);
static int lm_result_num_rows(lua_State *L);
static int lm_result_num_fields(lua_State *L);
static int lm_result_fetch_field(lua_State *L);
static int lm_result_fetch_field_direct(lua_State *L);
static int lm_result_fetch_fields(lua_State *L);
static int lm_result_fetch_lengths(lua_State *L);
static int lm_result_gc(lua_State *L);

/* statement functions */
static int lm_statement_init(lua_State *L);
static int lm_statement_prepare(lua_State *L);
static int lm_statement_bind(lua_State *L);
static int lm_statement_execute(lua_State *L);
static int lm_statement_fetch(lua_State *L);
static int lm_statement_reset(lua_State *L);
static int lm_statement_close(lua_State *L);
static int lm_statement_error(lua_State *L);
static int lm_statement_gc(lua_State *L);

