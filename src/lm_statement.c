
static void lm_statement_free_execute_memory(lm_statement *s) {
  if (s->res && s->result.bind) {
    int i;
    for (i = 0; i < s->result.n; i++) {
      if (s->result.bind[i].buffer) {
        free(s->result.bind[i].buffer);
      }
      s->result.bind[i].buffer = NULL;
    }
  }
  mysql_stmt_free_result(s->stmt);
}

static void lm_statement_free_memory(lm_statement *s) {
  if (s->param.bind) {
    free(s->param.bind);
    s->param.bind = NULL;
  }
  if (s->param.length) {
    free(s->param.length);
    s->param.length = NULL;
  }
  if (s->param.buffer) {
    free(s->param.buffer);
    s->param.buffer = NULL;
  }
  s->param.n = 0;
  if (s->res) {
    mysql_free_result(s->res);
    s->res = NULL;
  }
  if (s->result.bind) {
    int i;
    for (i = 0; i < s->result.n; i++) {
      if (s->result.bind[i].buffer)
        free(s->result.bind[i].buffer);
      s->result.bind[i].buffer = NULL;
    }
    free(s->result.bind);
    s->result.bind = NULL;
  }
  if (s->result.is_null) {
    free(s->result.is_null);
    s->result.is_null = NULL;
  }
  if (s->result.length) {
    free(s->result.length);
    s->result.length = NULL;
  }
  s->result.n = 0;
}

static int lm_statement_init(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  lm_statement *s = (lm_statement *)lua_newuserdata(L, sizeof(lm_statement));;
  my_bool yes = 1;
  lm_setmeta(L, LUAMYSQL_STATEMENT);
  s->param.n = 0;
  s->result.n = 0;
  if(c->closed) {
    lm_error(L, "connection is closed");
  }
  s->stmt = mysql_stmt_init(c->mysql);
  if(s->stmt == NULL) {
    lm_error(L, "out of memory");
  }
  if (mysql_stmt_attr_set(s->stmt, STMT_ATTR_UPDATE_MAX_LENGTH, &yes))
    lm_error(L, "mysql_stmt_attr_set() failed");
  return 1;
}

static lm_statement *lm_statement_get(lua_State *L) {
  lm_statement *s = (lm_statement *)luaL_checkudata(L, 1, LUAMYSQL_STATEMENT);
  return s;
}

/*
*  int mysql_stmt_prepare(MYSQL_STMT *stmt, const char *stmt_str, unsigned long length)
*/
static int lm_statement_prepare(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  lm_statement_free_execute_memory(s);

  size_t length;
  size_t i, n;
  MYSQL_FIELD *fields;
  const char *stmt_str = luaL_checklstring(L, 2, &length);
  if(mysql_stmt_prepare(s->stmt, stmt_str, length)) {
    lm_error(L, mysql_stmt_error(s->stmt));
  }
  /* prepare bind and param */
  n = mysql_stmt_param_count(s->stmt);
  if(n > 0) {
    s->param.n = n;
    s->param.bind = malloc(sizeof(s->param.bind[0]) * n);
    s->param.length = malloc(sizeof(s->param.length[0]) * n);
    s->param.buffer = malloc(sizeof(s->param.buffer[0]) * n);
    s->res = mysql_stmt_result_metadata(s->stmt);
    if (s->res) {
      n = s->result.n = mysql_num_fields(s->res);
      s->result.bind = malloc(sizeof(s->result.bind[0]) * n);
      s->result.is_null = malloc(sizeof(s->result.is_null[0]) * n);
      s->result.length = malloc(sizeof(s->result.length[0]) * n);
      memset(s->result.bind, 0, sizeof(s->result.bind[0]) * n);
      memset(s->result.is_null, 0, sizeof(s->result.is_null[0]) * n);
      memset(s->result.length, 0, sizeof(s->result.length[0]) * n);

      fields = mysql_fetch_fields(s->res);
      for (i = 0; i < n; i++) {
        s->result.bind[i].buffer_type = fields[i].type;
#if MYSQL_VERSION_ID < 50003
        if (field[i].type == MYSQL_TYPE_DECIMAL)
          s->result.bind[i].buffer_type = MYSQL_TYPE_STRING;
#endif
        s->result.bind[i].is_null = &(s->result.is_null[i]);
        s->result.bind[i].length = &(s->result.length[i]);
        s->result.bind[i].is_unsigned = ((fields[i].flags & UNSIGNED_FLAG) != 0);
      }
    }
    else {
      if (mysql_stmt_errno(s->stmt))
        luaL_error(L, "mysql_stmt_result_metadata failed");
    }
  }
  return 1;
}

/*
*  my_bool mysql_stmt_bind_param(MYSQL_STMT *stmt, MYSQL_BIND *bind)
*/

static int lm_statement_bind(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  MYSQL_STMT *stmt = s->stmt;
  int i;
  size_t argc = lua_gettop(L) - 1;
  int type;
  if (s->param.n != argc)
    luaL_error(L, "invalid argc");
  if (argc > 0) {
    memset(s->param.bind, 0, sizeof(s->param.bind[0])*argc);
    for(i = 0; i < argc; i++) {
      type = lua_type(L, i + 2);
      switch(type) {
        case LUA_TNIL:
          s->param.bind[i].buffer_type = MYSQL_TYPE_NULL;
          break;
        /* let the mysql handle the conversion of numbers */
        case LUA_TNUMBER:
        case LUA_TSTRING:
          s->param.bind[i].buffer = luaL_checklstring(L, i + 2, &s->param.length[i]);
          s->param.bind[i].buffer_type = MYSQL_TYPE_STRING;
          s->param.bind[i].length = &(s->param.length[i]);
          break;
        case LUA_TBOOLEAN:
          break;
        default:
          luaL_error(L, "unknown type");
      }
    }

    if (mysql_stmt_bind_param(stmt, s->param.bind)) {
      luaL_error(L, "mysql_stmt_bind_param failed");
    }
  }
  return 1;
}

static int lm_statement_execute(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  MYSQL_STMT *stmt = s->stmt;
  size_t i;

  if (mysql_stmt_execute(stmt)) {
    luaL_error(L, "mysql_stmt_execute failed");
  }
  if (s->res) {
    MYSQL_FIELD *fields;
    if (mysql_stmt_store_result(stmt))
      luaL_error(L, "mysql_stmt_store_result failed");
    fields = mysql_fetch_fields(s->res);
    s->fields = fields;
    for (i = 0; i < s->result.n; i++) {
      switch(s->result.bind[i].buffer_type) {
        case MYSQL_TYPE_NULL:
          break;
        case MYSQL_TYPE_TINY:
        case MYSQL_TYPE_SHORT:
        case MYSQL_TYPE_YEAR:
        case MYSQL_TYPE_INT24:
        case MYSQL_TYPE_LONG:
        case MYSQL_TYPE_LONGLONG:
        case MYSQL_TYPE_FLOAT:
        case MYSQL_TYPE_DOUBLE:
          s->result.bind[i].buffer = malloc(8);
          s->result.bind[i].buffer_length = 8;
          memset(s->result.bind[i].buffer, 0, 8);
          break;
        case MYSQL_TYPE_DECIMAL:
        case MYSQL_TYPE_STRING:
        case MYSQL_TYPE_VAR_STRING:
        case MYSQL_TYPE_TINY_BLOB:
        case MYSQL_TYPE_BLOB:
        case MYSQL_TYPE_MEDIUM_BLOB:
        case MYSQL_TYPE_LONG_BLOB:
#if MYSQL_VERSION_ID >= 50003
        case MYSQL_TYPE_NEWDECIMAL:
        case MYSQL_TYPE_BIT:
#endif
          s->result.bind[i].buffer = malloc(fields[i].max_length);
          memset(s->result.bind[i].buffer, 0, fields[i].max_length);
          s->result.bind[i].buffer_length = fields[i].max_length;
          break;
        case MYSQL_TYPE_TIME:
        case MYSQL_TYPE_DATE:
        case MYSQL_TYPE_DATETIME:
        case MYSQL_TYPE_TIMESTAMP:
          s->result.bind[i].buffer = malloc(sizeof(MYSQL_TIME));
          s->result.bind[i].buffer_length = sizeof(MYSQL_TIME);
          memset(s->result.bind[i].buffer, 0, sizeof(MYSQL_TIME));
          break;
        default:
          luaL_error(L, "uknown type!");
      }
    }
    if (mysql_stmt_bind_result(s->stmt, s->result.bind))
      luaL_error(L, "mysql_stmt_bind_result failed");
  }
  return 1;
}

static int lm_statement_fetch(lua_State *L) {
  int r, assoc;
  lm_statement *s = lm_statement_get(L);
  const char *type = luaL_optstring (L, 3, "n");
  assoc = strchr(type, 'a') ? 1 : 0;
  /*
  TODO: check if statement is open
  */
  r = mysql_stmt_fetch(s->stmt);
  if (r == MYSQL_NO_DATA) {
    lua_pushnil(L);
    return 1;
  }

#ifdef MYSQL_DATA_TRUNCATED
  if (r == MYSQL_DATA_TRUNCATED)
    luaL_error(L, "mysql_stmt_fetch data truncated");
#endif
  if (r == 1)
    luaL_error(L, "mysql_stmt_fetch failed");
  if (lua_istable(L, 2)) {
    /* Copy values to numerical indices */
    for (int i = 0; i < s->result.n; i++) {
      if(s->result.is_null[i]) {
        lua_pushnil(L);
      }
      else {
        /* handle per-type */
        MYSQL_TIME *t;
        char date_str[20]; /* size of yyyy-mm-dd hh:MM:ss */
        switch (s->result.bind[i].buffer_type) {
          case MYSQL_TYPE_TINY:
            if (s->result.bind[i].is_unsigned)
              lua_pushnumber(L, *(unsigned char *)s->result.bind[i].buffer);
            else
              lua_pushnumber(L, *(signed char *)s->result.bind[i].buffer);
            break;
          case MYSQL_TYPE_SHORT:
          case MYSQL_TYPE_YEAR:
            if (s->result.bind[i].is_unsigned)
              lua_pushnumber(L, *(unsigned short *)s->result.bind[i].buffer);
            else
              lua_pushnumber(L, *(short *)s->result.bind[i].buffer);
            break;
          case MYSQL_TYPE_INT24:
          case MYSQL_TYPE_LONG:
            if (s->result.bind[i].is_unsigned)
              lua_pushnumber(L, *(unsigned int *)s->result.bind[i].buffer);
            else
              lua_pushnumber(L, *(int *)s->result.bind[i].buffer);
            break;
          case MYSQL_TYPE_LONGLONG:
            if (s->result.bind[i].is_unsigned)
              lua_pushnumber(L,*(unsigned long long *)s->result.bind[i].buffer);
            else
              lua_pushnumber(L, *(long long *)s->result.bind[i].buffer);
            break;
          case MYSQL_TYPE_FLOAT:
            lua_pushnumber(L, *(float *)s->result.bind[i].buffer);
            break;
          case MYSQL_TYPE_DOUBLE:
            lua_pushnumber(L, *(double *)s->result.bind[i].buffer);
            break;
          case MYSQL_TYPE_DECIMAL:
          case MYSQL_TYPE_STRING:
          case MYSQL_TYPE_VAR_STRING:
          case MYSQL_TYPE_TINY_BLOB:
          case MYSQL_TYPE_BLOB:
          case MYSQL_TYPE_MEDIUM_BLOB:
          case MYSQL_TYPE_LONG_BLOB:
#if MYSQL_VERSION_ID >= 50003
          case MYSQL_TYPE_NEWDECIMAL:
          case MYSQL_TYPE_BIT:
#endif
            lua_pushlstring(L, s->result.bind[i].buffer, s->result.length[i]);
            break;
          case MYSQL_TYPE_TIME:
            t = (MYSQL_TIME *)s->result.bind[i].buffer;
            sprintf(date_str, "%02d:%02d:%02d", t->hour, t->minute, t->second);
            lua_pushstring(L, date_str);
            break;
          case MYSQL_TYPE_DATE:
            t = (MYSQL_TIME *)s->result.bind[i].buffer;
            sprintf(date_str, "%04d-%02d-%02d", t->year, t->month, t->day);
            lua_pushstring(L, date_str);
            break;
          case MYSQL_TYPE_DATETIME:
          case MYSQL_TYPE_TIMESTAMP:
            t = (MYSQL_TIME *)s->result.bind[i].buffer;
            sprintf(date_str, "%04d-%02d-%02d %02d:%02d:%02d", t->year, \
                t->month, t->day, t->hour, t->minute, t->second);
            lua_pushstring(L, date_str);
            break;
          default:
            luaL_error(L, "uknown type");
        }
      }
      if(!assoc) {
        lua_rawseti(L, 2, i + 1);
      }
      if(assoc) {
        lua_setfield(L, 2, s->fields[i].name);
      }
    }
    lua_pushvalue(L, 2);
  }

  return 1;
}

static int lm_statement_reset(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  if(s->param.n > 0) {
    lm_statement_free_execute_memory(s);
  }
  if(s->result.n > 0) {
    lm_statement_free_memory(s);
  }
  mysql_stmt_reset(s->stmt);
  return 1;
}

/*
* my_ulonglong mysql_stmt_affected_rows(MYSQL_STMT *stmt)
*/
static int lm_statement_affected_rows(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  my_ulonglong affected_rows = mysql_stmt_affected_rows(s->stmt);
  lua_pushnumber(L, affected_rows);
  return 1;
}

static int lm_statement_close(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  int res = mysql_stmt_close(s->stmt);
  if(res != 0) {
    lm_error(L, "statement reset failed");
  }
  lua_pushnumber(L, res);
  return 1;
}

/*
*  const char *mysql_stmt_error(MYSQL_STMT *stmt)
*/
static int lm_statement_error(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  const char *msg = mysql_stmt_error(s->stmt);
  lua_pushstring(L, msg);
  return 1;
}

/*
* unsigned int mysql_stmt_errno(MYSQL_STMT *stmt)
*/
static int lm_statement_errno(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  unsigned int errno = mysql_stmt_errno(s->stmt);
  lua_pushnumber(L, errno);
  return 1;
}

/*
*  unsigned int mysql_stmt_field_count(MYSQL_STMT *stmt)
*/
static int lm_statement_field_count(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  unsigned int field_count = mysql_stmt_field_count(s->stmt);
  lua_pushnumber(L, field_count);
  return 1;
}

/*
*  my_ulonglong mysql_stmt_insert_id(MYSQL_STMT *stmt)
*/
static int lm_statement_insert_id(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  my_ulonglong insert_id = mysql_stmt_insert_id(s->stmt);
  lua_pushnumber(L, insert_id);
  return 1;
}

/*
*  my_ulonglong mysql_stmt_num_rows(MYSQL_STMT *stmt)
*/
static int lm_statement_num_rows(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  my_ulonglong num_rows = mysql_stmt_num_rows(s->stmt);
  lua_pushnumber(L, num_rows);
  return 1;
}

/*
*  unsigned long mysql_stmt_param_count(MYSQL_STMT *stmt)
*/
static int lm_statement_param_count(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  unsigned long param_count = mysql_stmt_param_count(s->stmt);
  lua_pushnumber(L, param_count);
  return 1;
}

/*
*  MYSQL_RES mysql_stmt_result_metadata(MYSQL_STMT *stmt)
*/
static int lm_statement_result_metadata(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  lm_result *r = (lm_result*) lua_newuserdata(L, sizeof(lm_result));
  r->res = mysql_stmt_result_metadata(s->stmt);
  lm_setmeta(L, LUAMYSQL_RESULT);
  return 1;
}

static int lm_statement_gc(lua_State *L) {
  lm_statement *s = lm_statement_get(L);
  lm_statement_reset(L);
  mysql_stmt_close(s->stmt);
  return 1;
}

static const struct luaL_reg lm_statement_methods[] = {
  {"prepare", lm_statement_prepare},
  {"reset", lm_statement_reset},
  {"close", lm_statement_close},
  {"bind", lm_statement_bind},
  {"execute", lm_statement_execute},
  {"fetch", lm_statement_fetch},
  {"error", lm_statement_error},
  {"errno", lm_statement_errno},
  {"affected_rows", lm_statement_affected_rows},
  {"field_count", lm_statement_field_count},
  {"insert_id", lm_statement_insert_id},
  {"num_rows", lm_statement_num_rows},
  {"param_count", lm_statement_param_count},
  {"result_metadata", lm_statement_result_metadata},
  {"__gc", lm_statement_gc},
  {NULL, NULL}
};


