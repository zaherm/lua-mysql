
#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
/*
** Adapted from Lua 5.2.0
*/
static void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup) {
  luaL_checkstack(L, nup, "too many upvalues");
  for (; l->name != NULL; l++) {  /* fill the table with given functions */
    int i;
    for (i = 0; i < nup; i++)  /* copy upvalues to the top */
      lua_pushvalue(L, -nup);
    lua_pushstring(L, l->name);
    lua_pushcclosure(L, l->func, nup);  /* closure with those upvalues */
    lua_settable(L, -(nup + 3));
  }
  lua_pop(L, nup);  /* remove upvalues */
}
#endif

LUALIB_API int lm_error(lua_State *L, const char *message) {
  luaL_error(L, message);
  return 1;
}

LUALIB_API void lm_setmeta(lua_State *L, const char *name) {
  luaL_getmetatable(L, name);
  lua_setmetatable(L, -2);
}

/*
 ** Get the internal database type of the given field.
 */
static char *lm_get_field_type(enum enum_field_types type) {
  switch (type) {
    case MYSQL_TYPE_VAR_STRING: case MYSQL_TYPE_STRING:
      return "string";
    case MYSQL_TYPE_DECIMAL: case MYSQL_TYPE_SHORT: case MYSQL_TYPE_LONG:
    case MYSQL_TYPE_FLOAT: case MYSQL_TYPE_DOUBLE: case MYSQL_TYPE_LONGLONG:
    case MYSQL_TYPE_INT24: case MYSQL_TYPE_YEAR: case MYSQL_TYPE_TINY:
      return "number";
    case MYSQL_TYPE_TINY_BLOB: case MYSQL_TYPE_MEDIUM_BLOB:
    case MYSQL_TYPE_LONG_BLOB: case MYSQL_TYPE_BLOB:
      return "binary";
    case MYSQL_TYPE_DATE: case MYSQL_TYPE_NEWDATE:
      return "date";
    case MYSQL_TYPE_DATETIME:
      return "datetime";
    case MYSQL_TYPE_TIME:
      return "time";
    case MYSQL_TYPE_TIMESTAMP:
      return "timestamp";
    case MYSQL_TYPE_ENUM: case MYSQL_TYPE_SET:
      return "set";
    case MYSQL_TYPE_NULL:
      return "null";
    default:
      return "undefined";
  }
}

/*
** Push the value of #i field of #tuple row.
*/
static void lm_pushvalue(lua_State *L, void *row, long int length) {
  if (row == NULL)
    lua_pushnil(L);
  else
    lua_pushlstring(L, row, length);
}

/*
* push field table */
static void lm_pushfield(lua_State *L, int index, MYSQL_FIELD *field) {
  lua_newtable(L);
  lua_pushstring(L, field->name);
  lua_setfield(L, index, "name");
  lua_pushstring(L, field->org_name);
  lua_setfield(L, index, "org_name");
  lua_pushstring(L, field->table);
  lua_setfield(L, index, "table");
  lua_pushstring(L, field->org_table);
  lua_setfield(L, index, "org_table");
  lua_pushstring(L, field->db);
  lua_setfield(L, index, "db");
  lua_pushstring(L, field->catalog);
  lua_setfield(L, index, "catalog");
  lua_pushstring(L, field->def);
  lua_setfield(L, index, "def");
  lua_pushnumber(L, field->length);
  lua_setfield(L, index, "length");
  lua_pushnumber(L, field->max_length);
  lua_setfield(L, index, "max_length");
  lua_pushnumber(L, field->name_length);
  lua_setfield(L, index, "name_length");
  lua_pushnumber(L, field->org_name_length);
  lua_setfield(L, index, "org_name_length");
  lua_pushnumber(L, field->table_length);
  lua_setfield(L, index, "table_length");
  lua_pushnumber(L, field->org_table_length);
  lua_setfield(L, index, "org_table_length");
  lua_pushnumber(L, field->db_length);
  lua_setfield(L, index, "db_length");
  lua_pushnumber(L, field->catalog_length);
  lua_setfield(L, index, "catalog_length");
  lua_pushnumber(L, field->def_length);
  lua_setfield(L, index, "def_length");
  lua_pushnumber(L, field->flags);
  lua_setfield(L, index, "flags");
  lua_pushnumber(L, field->decimals);
  lua_setfield(L, index, "decimals");
  lua_pushnumber(L, field->charsetnr);
  lua_setfield(L, index, "charsetnr");
}

/*
*  unsigned long mysql_hex_string(char *to, const char *from, unsigned long length)
*/
static int lm_hex_string(lua_State *L) {
  size_t from_length, to_length;
  const char *from = luaL_checklstring(L, 1, &from_length);
  char *to;
  to = (char*)malloc(sizeof(char) * (2 * from_length + 1));
  to_length = mysql_hex_string(to, from, from_length);
  lua_pushlstring(L, to, to_length);
  free(to);
  return 1;
}

/*
*  const char *mysql_get_client_info(void)
*/
static int lm_get_client_info(lua_State *L) {
  const char *info = mysql_get_client_info();
  lua_pushstring(L, info);
  return 1;
}

/*
* unsigned long mysql_get_client_version(void)
*/
static int lm_get_client_version(lua_State *L) {
  unsigned long version = mysql_get_client_version();
  lua_pushnumber(L, version);
  return 1;
}

LUALIB_API int lm_createmeta(lua_State *L, const char *name, const luaL_Reg *methods) {
  if (!luaL_newmetatable(L, name)) {
    return 0;
  }

  luaL_setfuncs(L, methods, 0);

  /* define metamethods */
  lua_pushliteral(L, "__index");
  lua_pushvalue(L, -2);
  lua_settable(L, -3);

  /* define metamethods */
  lua_pushliteral(L, "_TYPE");
  lua_pushstring(L, name);
  lua_settable(L, -3);


/* TODO
  lua_pushliteral(L, "__tostring");
  lua_pushvalue(L, -2);
  lua_settable(L, -3);
*/
   /* disable access to __metatable */
  lua_pushliteral(L, "__metatable");
  lua_pushliteral(L, "you're not allowed to get this metatable");
  lua_settable(L, -3);

  return 1;
}


