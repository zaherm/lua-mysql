static int lm_result_init(lua_State *L) {
  lm_connection *m = lm_connection_get(L);
  lm_result *r = (lm_result*) lua_newuserdata(L, sizeof(lm_result));
  r->res = mysql_store_result(m->mysql);
  lm_setmeta(L, LUAMYSQL_RESULT);
  return 1;
}

static lm_result *lm_result_get(lua_State *L) {
  lm_result *r = (lm_result *)luaL_checkudata(L, 1, LUAMYSQL_RESULT);
  return r;
}

static int lm_result_fetch_row(lua_State *L) {
  lm_result *r = lm_result_get(L);
  if(r->res == NULL) {
    lm_error(L, "result is null");
  }
  MYSQL_ROW row = mysql_fetch_row(r->res);
  MYSQL_FIELD *fields;
  const char *type = luaL_optstring(L, 3, "n");
  int assoc = (strchr(type, 'a') != NULL) ? 1 : 0;
  if (row == NULL) {
    lua_pushnil(L); /* no more results */
    return 1;
  }
  if(assoc) {
    fields = mysql_fetch_fields(r->res);
  }
  unsigned int num_fields = mysql_num_fields(r->res);
  unsigned long *lengths = mysql_fetch_lengths(r->res);
  int i;
  if(lua_istable(L, 2)) {
    for(i = 0; i < num_fields; i++) {
      lm_pushvalue(L, row[i], lengths[i]);
      if(assoc)
        lua_setfield(L, 2, fields[i].name);
      else
        lua_rawseti(L, 2, i+1);
    }
  }
  lua_pushvalue(L, 2);
  return 1;
}

static int lm_result_free(lua_State *L) {
  lm_result *r = lm_result_get(L);
  mysql_free_result(r->res);
  return 1;
}


/*
*  my_ulonglong mysql_num_rows(MYSQL_RES *result)
*/

static int lm_result_num_rows(lua_State *L) {
  lm_result *r = lm_result_get(L);
  lua_pushnumber(L, mysql_num_rows(r->res));
  return 1;
}

/*
* unsigned int mysql_num_fields(MYSQL_RES *result)
*/

static int lm_result_num_fields(lua_State *L) {
  lm_result *r = lm_result_get(L);
  lua_pushnumber(L, mysql_num_fields(r->res));
  return 1;
}


/*
*  MYSQL_FIELD *mysql_fetch_field(MYSQL_RES *result)
*/

static int lm_result_fetch_field(lua_State *L) {
  lm_result *r = lm_result_get(L);

  if(r->res) {
    MYSQL_FIELD *field = mysql_fetch_field(r->res);
    if(field) {
      lm_pushfield(L, 2, field);
      lua_pushvalue(L, 2);
    }
  }
  else {
    lua_pushnil(L);
  }
  return 1;
}

/*
*  MYSQL_FIELD *mysql_fetch_field_direct(MYSQL_RES *result, unsigned int fieldnr)
*/

static int lm_result_fetch_field_direct(lua_State *L) {
  lm_result *r = lm_result_get(L);
  unsigned int fieldnr = luaL_checknumber(L, 2) - 1; /* indices start at 1 */
  if(r->res) {
    MYSQL_FIELD *field = mysql_fetch_field_direct(r->res, fieldnr);
    if(field) {
      lm_pushfield(L, 3, field);
      lua_pushvalue(L, 3);
    }
  }
  else {
    lua_pushnil(L);
  }
  return 1;
}

/*
*  MYSQL_FIELD *mysql_fetch_fields(MYSQL_RES *result)
*/

static int lm_result_fetch_fields(lua_State *L) {
  lm_result *r = lm_result_get(L);
  unsigned int i, num_fields;
  if(r->res) {
    num_fields = mysql_num_fields(r->res);
    MYSQL_FIELD *fields = mysql_fetch_fields(r->res);
    if(fields) {
      lua_newtable(L);
      for(i = 0; i < num_fields; i++) {
        lm_pushfield(L, 3, &fields[i]);
        lua_rawseti(L, -2, i + 1);
      }
      lua_pushvalue(L, 2);
    }
  }
  else {
    lua_pushnil(L);
  }
  return 1;
}

/*
*  unsigned long *mysql_fetch_lengths(MYSQL_RES *result)
*/

static int lm_result_fetch_lengths(lua_State *L) {
  lm_result *r = lm_result_get(L);
  unsigned long *lengths;
  unsigned int num_fields;
  unsigned int i;
  if(r->res) {
    num_fields = mysql_num_fields(r->res);
    lengths = mysql_fetch_lengths(r->res);
    lua_newtable(L);
    for(i = 0; i < num_fields; i++){
      lua_pushnumber(L, lengths[i]);
      lua_rawseti(L, -2, i + 1);
    }
    lua_pushvalue(L, 2);
  }
  return 1;
}

static int lm_result_gc(lua_State *L) {
  lm_result_free(L);
  return 1;
}


static const struct luaL_reg lm_result_methods[] = {
  {"fetch_row", lm_result_fetch_row},
  {"fetch_field", lm_result_fetch_field},
  {"fetch_field_direct", lm_result_fetch_field_direct},
  {"fetch_fields", lm_result_fetch_fields},
  {"fetch_lengths", lm_result_fetch_lengths},
  {"num_rows", lm_result_num_rows},
  {"num_fields", lm_result_num_fields},
  {"free", lm_result_free},
  {"__gc", lm_result_gc},
  {NULL, NULL}
};



