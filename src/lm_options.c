
static int lm_options_table(lua_State *L) {
  lua_newtable(L);
  int i;
  for(i = 0; lm_options_map[i].name != NULL; i++) {
    lua_pushstring(L, lm_options_map[i].name);
    switch (lm_options_map[i].lm_opt) {
      case LUAMYSQL_OPTION_NUMBER:
        lua_pushliteral(L, "number");
        break;
      case LUAMYSQL_OPTION_STRING:
        lua_pushliteral(L, "string");
        break;
      case LUAMYSQL_OPTION_BOOLEAN:
        lua_pushliteral(L, "boolean");
        break;
      case LUAMYSQL_OPTION_NULL:
        lua_pushliteral(L, "null");
        break;
    }
    lua_settable(L, -3);
  }
  lua_setfield(L, -2, "options");
  return 1;
}

