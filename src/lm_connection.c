
static int lm_connection_init(lua_State *L) {
  lm_connection *c = (lm_connection *)lua_newuserdata(L, sizeof(lm_connection));
  lm_setmeta(L, LUAMYSQL_CONNECTION);
  c->mysql = mysql_init(NULL);
  if(c->mysql == NULL) {
    lm_error(L, "insufficient memory");
  }
  if(lua_istable(L, 1)) {
    const char *key;
    char *s;
    unsigned int n;
    int i;
    lua_pushnil(L);  /* first key */
    while (lua_next(L, 1) != 0) {
      key = luaL_checkstring(L, -2);
      for(i = 0; lm_options_map[i].name != NULL; i++) {
        if(strcmp(key, lm_options_map[i].name) == 0) {
          switch(lm_options_map[i].lm_opt) {
            case LUAMYSQL_OPTION_NUMBER:
              n = (unsigned int)luaL_checkint(L, -1);
              mysql_options(c->mysql, lm_options_map[i].mysql_opt, &n);
              break;
            case LUAMYSQL_OPTION_NULL:
              mysql_options(c->mysql, lm_options_map[i].mysql_opt, NULL);
              break;
            case LUAMYSQL_OPTION_STRING:
              s = luaL_checkstring(L, -1);
              mysql_options(c->mysql, lm_options_map[i].mysql_opt, &s);
              break;
            case LUAMYSQL_OPTION_BOOLEAN:
              n = lua_toboolean(L, -1);
              mysql_options(c->mysql, lm_options_map[i].mysql_opt, &n);
              break;
            default:
              lm_error(L, "unkown option");
              break;
          }
        }
      }
      lua_pop(L, 1);
    }
  }
  c->closed = 1;
  return 1;
}

static lm_connection *lm_connection_get(lua_State *L) {
  lm_connection *c = (lm_connection *)luaL_checkudata(L, 1, LUAMYSQL_CONNECTION);
  luaL_argcheck(L, c != NULL, 1, "mysql expected");
  return c;
}

/*
*  MYSQL *mysql_real_connect(MYSQL *mysql, const char *host, const char *user, const char *passwd, const char *db, unsigned int port, const char *unix_socket, unsigned long client_flag)
*/

static int lm_connection_connect(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *host = luaL_optstring(L, 2, NULL);
  const char *user = luaL_optstring(L, 3, NULL);
  const char *passwd = luaL_optstring(L, 4, NULL);
  const char *db = luaL_optstring(L, 5, NULL);
  unsigned int port = luaL_optint(L, 6, 0);
  const char *unix_socket = luaL_optstring(L, 7, NULL);
  /* TODO: unsigned long client_flag = NULL; TODO */

  if(mysql_real_connect(c->mysql, host, user, passwd, db, port, unix_socket, CLIENT_REMEMBER_OPTIONS) == NULL){
    lm_error(L, "connection failed");
  }
  c->closed = 0;
  return 1;
}

/*
*  void mysql_close(MYSQL *mysql)
*/
static int lm_connection_close(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  if(!c->closed) {
    mysql_close(c->mysql);
    c->closed = 1;
  }
  lua_pushnumber(L, 0); /* success */
  return 1;
}

/*
*  my_bool mysql_commit(MYSQL *mysql)
*/
static int lm_connection_commit(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  my_bool res;
  res = mysql_commit(c->mysql);
  if(res != 0) {
    lm_error(L, "commit failed");
  }

  return 1;
}

/*
*  const char *mysql_error(MYSQL *mysql)
*/
static int lm_connection_error(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char* err = mysql_error(c->mysql);
  /* mysql_error return empty string or NULL terminated, so its safe to push string */
  lua_pushstring(L, err);
  return 1;
}

/*
*  unsigned int mysql_errno(MYSQL *mysql)
*/
static int lm_connection_errno(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  int errno = mysql_errno(c->mysql);
  lua_pushnumber(L, errno);
  return 1;
}

/*
*  int mysql_query(MYSQL *mysql, const char *stmt_str)
*/
static int lm_connection_query(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *stmt_str = luaL_checkstring(L, 2);
  if(stmt_str == NULL) {
    lm_error(L, "empty query");
  }
  int res = mysql_query(c->mysql, stmt_str);
  lua_pushnumber(L, res);
  return 1;
}

/*
*  int mysql_ping(MYSQL *mysql)
*/

static int lm_connection_ping(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  int res = mysql_ping(c->mysql);
  lua_pushnumber(L, res);
  return 1;
}
/*
*  unsigned long mysql_real_escape_string(MYSQL *mysql, char *to, const char *from, unsigned long length)
*/
static int lm_connection_escape(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  size_t size, new_size;
  const char *from = luaL_checklstring(L, 2, &size);
  char *to;
  to = (char*)malloc(sizeof(char) * (2 * size + 1));
  if(to) {
    new_size = mysql_real_escape_string(c->mysql, to, from, size);
    lua_pushlstring(L, to, new_size);
    free(to);
  }
  else {
    lm_error(L, "could not allocate escaped string");
  }
  return 1;
}

/*
*  my_ulonglong mysql_insert_id(MYSQL *mysql)
*/

static int lm_connection_insert_id(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  lua_pushnumber(L, mysql_insert_id(c->mysql));
  return 1;
}

/*
*  my_ulonglong mysql_affected_rows(MYSQL *mysql)
*/

static int lm_connection_affected_rows(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  lua_pushnumber(L, mysql_affected_rows(c->mysql));
  return 1;
}

/*
*  my_ulonglong mysql_autocommit(MYSQL *mysql)
*/

static int lm_connection_autocommit(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  int mode = lua_toboolean(L, 2);
  lua_pushnumber(L, mysql_autocommit(c->mysql, mode));
  return 1;
}

/*
*  my_bool mysql_rollback(MYSQL *mysql)
*/

static int lm_connection_rollback(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  lua_pushnumber(L, mysql_rollback(c->mysql));
  return 1;
}

/*
* const char *mysql_character_set_name(MYSQL *mysql)
*/

static int lm_connection_character_set_name(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *character_set_name = mysql_character_set_name(c->mysql);
  if(character_set_name != NULL) {
    lua_pushstring(L, character_set_name);
  }
  else {
    lua_pushnil(L);
  }
  return 1;
}

/*
* my_bool mysql_ssl_set(MYSQL *mysql, const char *key, const char *cert, const char *ca, const char *capath, const char *cipher)
*/

static int lm_connection_ssl_set(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *key = luaL_optstring(L, 2, NULL);
  const char *cert = luaL_optstring(L, 3, NULL);
  const char *ca = luaL_optstring(L, 4, NULL);
  const char *capath = luaL_optstring(L, 5, NULL);
  const char *cipher = luaL_optstring(L, 6, NULL);

  my_bool res = mysql_ssl_set(c->mysql, key, cert, ca, capath, cipher);
  lua_pushnumber(L, (int)res);
  return 1;
}

/*
*  int mysql_select_db(MYSQL *mysql, const char *db)
*/
static int lm_connection_select_db(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *db = luaL_checkstring(L, 2);
  int res = mysql_select_db(c->mysql, db);
  lua_pushnumber(L, res);
  return 1;
}

/*
* const char *mysql_stat(MYSQL *mysql)
*/
static int lm_connection_stat(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *stat = mysql_stat(c->mysql);
  if(stat == NULL)
    lua_pushnil(L);
  else
    lua_pushstring(L, stat);
  return 1;
}

/*
* const char *mysql_sqlstate(MYSQL *mysql)
*/
static int lm_connection_sqlstate(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *state = mysql_sqlstate(c->mysql);
  lua_pushstring(L, state);
  return 1;
}

/*
*  const char *mysql_get_host_info(MYSQL *mysql)
*/
static int lm_connection_get_host_info(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *host_info = mysql_get_host_info(c->mysql);
  lua_pushstring(L, host_info);
  return 1;
}

/*
*   unsigned int mysql_get_proto_info(MYSQL *mysql)
*/
static int lm_connection_get_proto_info(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  unsigned int proto_info = mysql_get_proto_info(c->mysql);
  lua_pushnumber(L, proto_info);
  return 1;
}

/*
* const char *mysql_get_server_info(MYSQL *mysql)
*/
static int lm_connection_get_server_info(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *server_info = mysql_get_server_info(c->mysql);
  lua_pushstring(L, server_info);
  return 1;
}

/*
*  unsigned long mysql_get_server_version(MYSQL *mysql)
*/
static int lm_connection_get_server_version(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  unsigned long server_version = mysql_get_server_version(c->mysql);
  lua_pushnumber(L, server_version);
  return 1;
}

/*
*  const char *mysql_get_ssl_cipher(MYSQL *mysql)
*/
static int lm_connection_get_ssl_cipher(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *ssl_cipher = mysql_get_ssl_cipher(c->mysql);
  if(ssl_cipher == NULL)
    lua_pushnil(L);
  else
    lua_pushstring(L, ssl_cipher);
  return 1;
}

/*
*  const char *mysql_info(MYSQL *mysql)
*/
static int lm_connection_info(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  const char *info = mysql_info(c->mysql);
  if(info == NULL)
    lua_pushnil(L);
  else
    lua_pushstring(L, info);
  return 1;
}

/*
*  unsigned int mysql_field_count(MYSQL *mysql)
*/
static int lm_connection_field_count(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  unsigned int count = mysql_field_count(c->mysql);
  lua_pushnumber(L, count);
  return 1;
}

/*
*  void mysql_get_character_set_info(MYSQL *mysql, MY_CHARSET_INFO *cs)
*/
static int lm_connection_get_character_set_info(lua_State *L) {
  lm_connection *c = lm_connection_get(L);
  MY_CHARSET_INFO cs;
  mysql_get_character_set_info(c->mysql, &cs);
  lua_newtable(L);
  lua_pushnumber(L, cs.number);
  lua_setfield(L, 2, "number");
  lua_pushstring(L, cs.name);
  lua_setfield(L, 2, "name");
  lua_pushstring(L, cs.csname);
  lua_setfield(L, 2, "csname");
  lua_pushstring(L, cs.comment);
  lua_setfield(L, 2, "comment");
  lua_pushstring(L, cs.dir);
  lua_setfield(L, 2, "dir");
  lua_pushnumber(L, cs.mbminlen);
  lua_setfield(L, 2, "mbminlen");
  lua_pushnumber(L, cs.mbmaxlen);
  lua_setfield(L, 2, "mbmaxlen");
  lua_pushvalue(L, 2);
  return 1;
}

static int lm_connection_gc(lua_State *L) {
  lm_connection_close(L);
  lua_pushnumber(L, 1);
  return 1;
}

static const struct luaL_reg lm_connection_methods[] = {
  {"connect", lm_connection_connect},
  {"close", lm_connection_close},
  {"commit", lm_connection_commit},
  {"autocommit", lm_connection_autocommit},
  {"rollback", lm_connection_rollback},
  {"error", lm_connection_error},
  {"errno", lm_connection_errno},
  {"query", lm_connection_query},
  {"ping", lm_connection_ping},
  {"escape", lm_connection_escape},
  {"insert_id", lm_connection_insert_id},
  {"affected_rows", lm_connection_affected_rows},
  {"character_set_name", lm_connection_character_set_name},
  {"ssl_set", lm_connection_ssl_set},
  {"select_db", lm_connection_select_db},
  {"stat", lm_connection_stat},
  {"sqlstate", lm_connection_sqlstate},
  {"get_character_set_info", lm_connection_get_character_set_info},
  {"get_host_info", lm_connection_get_host_info},
  {"get_proto_info", lm_connection_get_proto_info},
  {"get_server_info", lm_connection_get_server_info},
  {"get_server_version", lm_connection_get_server_version},
  {"get_ssl_cipher", lm_connection_get_ssl_cipher},
  {"info", lm_connection_info},
  {"field_count", lm_connection_field_count},
  /* to allow connection:result(), connection:statement() */
  {"result", lm_result_init},
  {"statement", lm_statement_init},
  {"__gc", lm_connection_gc},
  {NULL, NULL}
};

